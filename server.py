#!/usr/bin/python
'''
        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2019 Florian Schmidt <hi@f012.dev>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
'''

import socket

udpsocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)

udpsocket.bind(("0.0.0.0", 6000))

print "Bound to 0.0.0.0:6000"

data = None

while True:
    recv = udpsocket.recvfrom(8192)
    if not data:
        print "Storing connection"
        data = recv[1]
    else:
        print "Sending Connection"
        udpsocket.sendto("%s:%s" % data, recv[1])
        udpsocket.sendto("%s:%s" % recv[1], data)
        data = None
