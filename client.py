#!/usr/bin/python
'''
        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2019 Florian Schmidt <hi@f012.dev>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
'''

import socket
import time


udpsocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)


udpsocket.sendto("Hello", ("116.203.141.5", 6000))

while True:
    print "waiting ..."
    recv = udpsocket.recvfrom(8192)
    print recv
    if ":" in recv[0]:
        addr = recv[0].split(':')
        ip = addr[0]
        port = int(addr[1])
        combine = (ip, port)
        print "Sending proof to %s:%s" % combine
        udpsocket.sendto("Proof", combine)
        udpsocket.sendto("Proof", combine)
